# Ultra Utils 10000

A collection of utilities for ruby projects which I made.
For now this is basically just `rubocop` wrappers and a couple of small utilities.

# What is in the package

- [`list-changed-files`](#list-changed-files)\
  A tool to variously list changed files using git.
- [`lint`](#lint)\
  A wrapper around `rubocop` which runs it only on files which changed in this branch, index and working tree.
- [`git-hook-lint`](#git-hook-lint)\
  A wrapper around `rubocop` which can be used as a git pre-commit or pre-push hook.
- [`csv-split`](#csv-split)\
  A tool to quickly split CSV files into multiple.
- [`until-fail`](#until-fail)\
  Tiny script to loop execution until it returns with an error code.

# How to use

Add utils to your PATH in .(zsh|bash)rc:
```sh
PATH=/path/to/utils:$PATH
```

Restart shell:
```sh
exec $SHELL
```

### `list-changed-files`

This tool lists added and modified files in various places, while avoiding deleted files:
- staged (added and modified in index)
- unstaged (modified in working tree)
- uncommitted (combination of the above)
- untracked (added files in working tree)
- committed (added and changed files in comparison to first of `develop`, `master`, `main`)

**Examples**

- List all files:
  ```sh
  list-changed-files
  ```
- List added and modified files in working tree:
  ```sh
  list-changed-files -l unstaged,untracked
  ```
  You can use abbreviated names:
  ```sh
  list-changed-files -l uns,unt
  ```
- Separate file names with NUL:
  ```sh
  list-changed-files -z | xargs -0 stat
  ```
- Compare with a specific branch:
  ```sh
  list-changed-files -b epic_branch
  ```

**Known limitations**

If a file is deleted/moved and change is not committed, it will still be listed in committed files. The same goes for staged files. When selecting several lists, including default mode, such files will be present.

### `lint`

Run `rubocop` on changed ruby files. All parameters are passed directly to it. If using bundler, it will run `bundle exec rubocop`, plain `rubocop` otherwise.

Following file name globs are considered ruby files:
- *.rb
- *.rake
- Gemfile

It should be safe to run this with files containing any characters in file name, including spaces and new lines. But other tools may break, including git.

**Examples**

- Just check changed files:
  ```sh
  > lint
  ```
- Autocorrect everything:
  ```sh
  > lint -A
  ```

**Known limitations**

Files are detected by file name, so possibly not every file is checked.

Can only be run at repository root, as files are listed relative to it.

Also see limitations for `list-changed-files`.

### `git-hook-lint`

Run `rubocop` on changed ruby files and abort action if problems are detected. If using bundler, it will run `bundle exec rubocop`, plain `rubocop` otherwise. Only considers files relevant to the action.

Suitable for a pre-push or pre-commit hook.

**Usage**

Set as a hook:
```sh
> ln -s `which git-hook-lint` .git/hooks/pre-push
```
Important: if using link with relative path, it should be relative to hooks directory. Better just to use absolute paths.

Use git as normal:
```sh
> git add my_class.rb
> git commit -m "Added MyClass"
> git push
```

Wait for rubocop to finish:
```
Inspecting 1 file
C

Offenses:

my_class.rb:1:1: C: Style/Documentation: Missing top-level class documentation comment.
class MyClass
^^^^^
my_class.rb:1:1: C: [Correctable] Style/FrozenStringLiteralComment: Missing frozen string literal comment.
class MyClass
^

1 file inspected, 2 offenses detected, 1 offense auto-correctable

Action aborted
```

If there were *any* problems, push will be aborted.

Using as a pre-commit hook is similar, but staged files will be checked instead of committed.

**Known limitations**

Default is to run as a pre-push hook. When `$0` is `pre-commit`, behavior is changed. This means that it can't be used in a custom pre-commit hook without a name change.

Also see limitations for `lint`.

### `csv-split`

Open a large CSV file and split it into multiple numbered files. Works line-by-line, so it is fairly efficient.
Every file gets its own copy of the headers.

**Examples**

```sh
> csv-split 20000_lines_under_the_sea.csv 9000
> ls 20000*
20000_lines_under_the_sea.csv
20000_lines_under_the_sea_1.csv
20000_lines_under_the_sea_2.csv
20000_lines_under_the_sea_3.csv
> wc -l 20000_lines_under_the_sea_3.csv
    2001 20000_lines_under_the_sea_3.csv
```

### `until-fail`

Run a command until it exit with a non-zero status.
Useful for detecting flaky tests with `rubocop` or in similar situations.

**Examples**

```sh
rspec path/to/spec.rb
until-fail !!
```
